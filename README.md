#Configuration, Endpoints, and Front-end
Use following Commands for preloaded DB:

php artisan migrate 

php artisan db:seed

API Endpoints

http://sai_quiz_task.test/api/quiz/ (Provides a list of Quizzes)

http://sai_quiz_task.test/api/questions/1 (Provides a list of all Questions and relevant Answers of Given Quiz)

http://sai_quiz_task.test/api/question/1 (Provides paginated 1 Question per request and relevant Answers from the provided quiz_id, used for front-end implementation)

Front End Accessible URLs:

http://sai_quiz_task.test (vue-based front-end, showing the questions along with answers)
