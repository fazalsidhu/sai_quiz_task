<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    public function answer()
    {
        return $this->hasMany('App\Answer');
        // return $this->hasMany('App\Answer')->select(
        //     "id",
        //     "title",
        //     "fate",
        //     "created_at" //I wasn't selecting this guy
        // );
    }
}
