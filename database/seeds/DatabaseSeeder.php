<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(QuizTableSeeder::class);
        $this->call(QuestionTableSeeder::class);
        $this->call(ClientTableSeeder::class);
        // need to ensure at least one answer is correct, not ensuring presently
        for ($i = 1; $i < 6; $i++) {
            for ($j = 1; $j < 6; $j++) {
                DB::table('answers')->insert([
                    'question_id' => $i,
                    'title' => Str::random(5),
                    'fate' => rand(0, 1),
                    'status' => 1,
                    'created_at' => "2019-03-13 04:01:53",
                    'updated_at' => "2019-03-13 04:01:53"
                ]);
            }
        }

        // $this->call(AnswerTableSeeder::class);
    }
}
