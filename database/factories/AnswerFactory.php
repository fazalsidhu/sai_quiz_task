<?php

use Faker\Generator as Faker;

$factory->define(App\Answer::class, function (Faker $faker) {
    return [
        'question_id' => 1,
        'title' => $faker->text(5),
        'fate' => 0,
        'status' => 1,
    ];
});
