<?php

use Faker\Generator as Faker;

$factory->define(App\Client::class, function (Faker $faker) {
    return [
        'title' => $faker->text(10),
        'token' => Str::random(20)
    ];
});
