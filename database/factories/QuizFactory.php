<?php

use Faker\Generator as Faker;

$factory->define(App\Quiz::class, function (Faker $faker) {
    return [
        'title' => $faker->text(50)
    ];
});
// $factory->define(App\Question::class, function (Faker $faker) {
//     return [
//         //'quiz_id' => 1,
//         'title' => $faker->text(50)
//     ];
// });
