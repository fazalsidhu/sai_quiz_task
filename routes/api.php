<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// List Quiz
Route::get('quiz', 'QuizController@index');

// Adds Answer from the User
Route::post('participation', 'ParticipationController@store');

// List all Questions and relevant Answers of Given Quiz
Route::get('questions/{id}', 'QuestionController@show');

// Provides 1 Question per request and relevant Answers, used for front-end implementation
Route::get('question/{id}', 'QuestionController@index');
